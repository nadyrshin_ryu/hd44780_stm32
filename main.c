//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stdlib.h>
#include <string.h>
#include "stm32f10x.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "main.h"
#include "delay\delay.h"
#include "hd44780\hd44780.h"


unsigned char code = 0;

void main()
{
  SystemInit();

  hd44780_init();

  while (1)
  {
    hd44780_goto_xy(0, 0);

    hd44780_printf("  �����������\r\n  � ���������\r\n  ����1\r\n  ����2");
    
    delay_ms(500);
  }
}
